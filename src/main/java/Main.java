
import events.*;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;

import javax.security.auth.login.LoginException;

import java.io.IOException;
import java.util.Properties;

public class Main {
    public static void main(String[] args) throws LoginException, IOException {
        Properties prop = new Properties();
        prop.load(Main.class.getResourceAsStream("config.properties"));
        JDA builder = new JDABuilder(prop.getProperty("key")).build();

        builder.addEventListener(new RankedMatchCreate());
        builder.addEventListener(new RankedMatchResult());
        builder.addEventListener(new MakeTeam());
        builder.addEventListener(new EightBall());
        builder.addEventListener(new RankedRematch());
        builder.addEventListener(new ShowCommands());
        builder.addEventListener(new RankedVoiceGame());
        builder.addEventListener(new RankedStats());

        //TEST DATA
//        EntityDao dao = new EntityDao();
//
//        Player player = new Player("Simon", 150);
//        dao.savePlayer(player);
//
//        Player player2 = new Player("Kingen", 75);
//        dao.savePlayer(player2);
//
//        Player player4 = new Player("Dingen", 100);
//        dao.savePlayer(player4);
//
//        Player player3 = new Player("Pingen", 124);
//        dao.savePlayer(player3);
//
//        Player player5 = new Player("Ett", 90);
//        dao.savePlayer(player5);
//
//        Player player6 = new Player("Två", 130);
//        dao.savePlayer(player6);

//        eloMatch.addPlayer("Player 1", 1, 1050);
//        eloMatch.addPlayer("Player 2", 1, 1090);
//        eloMatch.addPlayer("Player 3", 1, 1000);
//
//        eloMatch.addPlayer("Player 4", 2, 500);
//        eloMatch.addPlayer("Player 5", 2, 1000);
//        eloMatch.addPlayer("Player 6", 2, 100);
//
//        eloMatch.calculateELOs();
//        System.out.println(eloMatch.getELO("Player 1"));
//        System.out.println(eloMatch.getELO("Player 2"));
//        System.out.println(eloMatch.getELO("Player 3"));
//
//        System.out.println(eloMatch.getELO("Player 5"));
//        System.out.println(eloMatch.getELO("Player 6"));
//        System.out.println(eloMatch.getELO("Player 4"));
//        System.out.println(eloMatch.getELOChange("Player 5"));
    }
}
