package elo;

import elo.entitities.Game;
import elo.entitities.Player;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateUtil;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.List;

public class EntityDao {
    public void savePlayer(Player player) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // save the player object
            session.save(player);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

    }

    public void saveGame(Game game) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.save(game);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public List<Player> getPlayers() {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            return session.createQuery("from Player", Player.class).list();
        }
    }

    public Game getGameById(Long gameId) {
        Session session = null;
        Game game = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            game = session.load(Game.class, gameId);
            Hibernate.initialize(game);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return game;
    }

    public Player getPlayerById(String playerId) {
        Player player = null;
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()){
//            transaction = session.beginTransaction();
//            session = HibernateUtil.getSessionFactory().openSession();
//            player = (Player) session.createQuery("from Player where name=:name").getSingleResult();
//            Query query = HibernateUtil.getSessionFactory().getCurrentSession().createQuery("from Player where name=:name");
//            query.setParameter("name", playerId);
//            Query query = HibernateUtil.getSessionFactory().getCurrentSession().createQuery("from Player where name=:name");
//            query.setParameter("name", playerId);
//
//            CriteriaBuilder builder = session.getCriteriaBuilder();
//            CriteriaQuery<Player> criteriaQuery = builder.createQuery(Player.class);
//            Root<Player> from = criteriaQuery.from(Player.class);
//            criteriaQuery.select(from);
//            ParameterExpression<String> play = builder.parameter(String.class);
//            criteriaQuery.where(builder.gt(from.get("name"), play));
            transaction = session.beginTransaction();
//            List results = HibernateUtil.getSessionFactory().getCurrentSession().createQuery("from Player where name=:name")
//                    .setParameter("name", playerId).list();
//            player = (Player) results.get(0);
//            transaction.commit();
//
//            String hql = "SELECT Player WHERE name = :name";
//            Query query = session.createQuery(hql);
//            query.setParameter("name", playerId);
//            player = (Player) query.getSingleResult();
//            player = (Player) query.getSingleResult();

//            player = (Player) query.getSingleResult();
//            player = session.load(Player.class, playerId);
//            Hibernate.initialize(player);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return player;
    }

    public void updatePlayer(Player player) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();

            // save the student object
            String hql = "UPDATE Player set elo = :newElo " + "WHERE id = :playerId";
            Query query = session.createQuery(hql);
            query.setParameter("newElo", player.getElo());
            query.setParameter("playerId", player.getId());
            int result = query.executeUpdate();

            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public void updateGame(Game game) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();

            // save the student object
            String hql = "UPDATE Game set gameDone = :isDone " + "WHERE id = :gameId";
            Query query = session.createQuery(hql);
            query.setParameter("isDone", game.isGameDone());
            query.setParameter("gameId", game.getId());
            int result = query.executeUpdate();

            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

}
