package elo.entitities;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "game")
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;

    @Column
    boolean gameDone = false;

    @ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinTable(name = "firstTeam",
            joinColumns = {@JoinColumn(name = "team_id")},
            inverseJoinColumns = {@JoinColumn(name = "player_id")}
    )
    private List<Player> teamOnePlayers = new ArrayList<>();

    @ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinTable(name = "secondTeam",
            joinColumns = {@JoinColumn(name = "team_id")},
            inverseJoinColumns = {@JoinColumn(name = "player_id")}
    )
    private List<Player> teamTwoPlayers = new ArrayList<>();


    public Game(){}

    public Game(List[] teams) {
       teamOnePlayers =  teams[0];
       teamTwoPlayers = teams[1];
    }

    public List<Player> getTeamOnePlayers() {
        return teamOnePlayers;
    }

    public void setTeamOnePlayers(List<Player> teamOnePlayers) {
        this.teamOnePlayers = teamOnePlayers;
    }

    public List<Player> getTeamTwoPlayers() {
        return teamTwoPlayers;
    }

    public void setTeamTwoPlayers(List<Player> teamTwoPlayers) {
        this.teamTwoPlayers = teamTwoPlayers;
    }

    public boolean isGameDone() {
        return gameDone;
    }

    public void setGameDone(boolean gameDone) {
        this.gameDone = gameDone;
    }

    public Long getId() {
        return id;
    }
}
