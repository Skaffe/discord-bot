package events;

import elo.EntityDao;
import elo.entitities.Game;
import elo.entitities.Player;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.List;

public class RankedVoiceGame extends ListenerAdapter {
    RankedMatchCreate rankedMatchCreate = new RankedMatchCreate();
    EntityDao dao = new EntityDao();

    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] messageSent = event.getMessage().getContentRaw().split(" ");

        if (messageSent[0].equalsIgnoreCase(".voice")) {
            if (!event.getMember().getUser().isBot()) {
                String channel = messageSent[1];
                Guild guild = event.getGuild();
                List<Member> members = guild.getVoiceChannelsByName(channel, true).get(0).getMembers();
                if (members.size() > 1) {
                    String[] connectedMembers = new String[members.size()];
                    for (int i = 0; i < members.size(); i++) {
                        connectedMembers[i] = members.get(i).getAsMention();
                    }

                    List<Player> playerList = rankedMatchCreate.addingPlayersToList(connectedMembers);

                    String playerScores = channel + "-game with Player pool: ";
                    for (Player player : playerList) {
                        playerScores += player.getName() + ": " + player.getElo() + ", ";
                    }
                    event.getChannel().sendMessage(playerScores).queue();
                    List[] bestCombo = rankedMatchCreate.createGame(playerList, event);

                    Game game = new Game(bestCombo);
                    dao.saveGame(game);
                    event.getChannel().sendMessage("> Game id: " + game.getId()).queue();
                    event.getChannel().sendMessage("> Write **.winner " + game.getId() + " winner(1 or 2)** to update results").queue();
                }

                else{
                    event.getChannel().sendMessage("> Not enough players").queue();
                }
            }
        }
    }
}
