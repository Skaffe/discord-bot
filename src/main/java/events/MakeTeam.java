package events;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MakeTeam extends ListenerAdapter {

    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] messageSent = event.getMessage().getContentRaw().split(" ");
        if (messageSent[0].equalsIgnoreCase(".teams")) {
            if (!event.getMember().getUser().isBot()) {
                String[] arrayWithoutCommand = Arrays.copyOfRange(messageSent, 1, messageSent.length);
                List<String> finalList = Arrays.asList(arrayWithoutCommand);
                Collections.shuffle(finalList);

                List[] teams = split(finalList);
                event.getChannel().sendMessage("> " + teams[0] + " vs " + teams[1]).queue();
            }
        }
    }

    public static List[] split(List<String> list){
        int size = list.size();

        List<String> firstTeam = new ArrayList<>(list.subList(0, (size/2)));
        List<String> secondTeam = new ArrayList<>(list.subList((size / 2), size));

        return new List[] {firstTeam, secondTeam};
    }
}
