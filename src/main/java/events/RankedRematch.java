package events;

import elo.EntityDao;
import elo.entitities.Game;
import elo.entitities.Player;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.ArrayList;
import java.util.List;

public class RankedRematch extends ListenerAdapter {
    EntityDao dao = new EntityDao();
    RankedMatchCreate rankedMatchCreate = new RankedMatchCreate();
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] messageSent = event.getMessage().getContentRaw().split(" ");

        if (messageSent[0].equalsIgnoreCase(".rematch")) {
            if (!event.getMember().getUser().isBot()) {
                Long id = Long.parseLong(messageSent[1]);
                Game game = dao.getGameById(id);

                List<Player> playerList = new ArrayList<>(game.getTeamOnePlayers());
                playerList.addAll(game.getTeamTwoPlayers());

                String playerScores = "Remaking game with Player pool: ";
                for (Player player : playerList) {
                    playerScores += player.getName() + ": " + player.getElo() + ", ";
                }
                event.getChannel().sendMessage(playerScores).queue();

                List[] bestCombo = rankedMatchCreate.createGame(playerList, event);

                Game newGame = new Game(bestCombo);

                dao.saveGame(newGame);
                event.getChannel().sendMessage("> Game id: " + newGame.getId()).queue();
                event.getChannel().sendMessage("> Write **.winner " + newGame.getId() + " winner(1 or 2)** to update results").queue();
            }

        }
    }

}