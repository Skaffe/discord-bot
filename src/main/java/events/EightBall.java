package events;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class EightBall extends ListenerAdapter {
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        List<String> answers = Arrays.asList("It is certain", "It is decidedly so.", "Without a doubt.", "Yes - definitely",
                "You may rely on it", "As I see it, yes.", "Most likely.", "Outlook good.", "Yes.", "Signs point to yes.",
                "Reply hazy, try again.", "Ask again later.", "Better not tell you now", "Cannot predict now", "Concentrate and ask again.", "Don't count on it.", "My reply is no",
                "My sources say no.", "Outlook not so good", "Very doubtful.");

        String[] messageSent = event.getMessage().getContentRaw().split(" ");
        String question = "";
        if (messageSent[0].equalsIgnoreCase(".8ball")) {
            if (!event.getMember().getUser().isBot()) {
                for (int i = 1; i < messageSent.length; i++) {
                    question = question + messageSent[i] + " ";
                }

                Random rand = new Random();
                String answer = answers.get(rand.nextInt(answers.size()));

                event.getChannel().sendMessage("> " + answer).queue();

            }
        }

    }
}
