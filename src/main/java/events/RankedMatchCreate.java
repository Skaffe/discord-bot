package events;

import elo.EntityDao;
import elo.entitities.Game;
import elo.entitities.Player;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.swing.text.html.parser.Entity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RankedMatchCreate extends ListenerAdapter {
    EntityDao dao = new EntityDao();

    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] messageSent = event.getMessage().getContentRaw().split(" ");

        if (messageSent[0].equalsIgnoreCase(".ranked")) {
            if (!event.getMember().getUser().isBot()) {
                String[] arrayWithoutCommand = Arrays.copyOfRange(messageSent, 1, messageSent.length);

                List<Player> playerList = addingPlayersToList(arrayWithoutCommand);

                String playerScores = "Player pool: ";
                for (Player player : playerList) {
                    playerScores += player.getName() + ": " + player.getElo() + ", ";
                }
                event.getChannel().sendMessage(playerScores).queue();

                List[] bestCombo = createGame(playerList, event);

                Game game = new Game(bestCombo);
                dao.saveGame(game);
                event.getChannel().sendMessage("> Game id: " + game.getId()).queue();
                event.getChannel().sendMessage("> Write **.winner " + game.getId() + " winner(1 or 2)** to update results").queue();

            }
        }
    }

    public List[] createGame(List<Player> playerList, GuildMessageReceivedEvent event){
        int goalTeamELO = getTotalElo(playerList) / 2;

        int teamOneClosestElo = 0;
        int teamTwoClosestElo = 0;
        List[] bestCombo = new List[0];
        for (int i = 0; i < 100; i++) {
            List<Player> placeholder = new ArrayList<>(playerList);
            Collections.shuffle(placeholder);

            List[] splittedTeams = split(placeholder);
            int teamOne = 0, teamTwo = 0;
            for (int teamIndex = 0; teamIndex < splittedTeams.length; teamIndex++) {

                for (int playerIndex = 0; playerIndex < splittedTeams[teamIndex].size(); playerIndex++) {
                    if (teamIndex == 0) {
                        Player player = (Player) splittedTeams[teamIndex].get(playerIndex);
                        teamOne += player.getElo();

                    } else {
                        Player player = (Player) splittedTeams[teamIndex].get(playerIndex);
                        teamTwo += player.getElo();
                    }
                }
            }
            if (Math.abs(goalTeamELO - teamOne) < Math.abs(goalTeamELO - teamOneClosestElo)) {
                bestCombo = splittedTeams;
                teamOneClosestElo = teamOne;
                teamTwoClosestElo = teamTwo;
            }

        }

        for (int i = 0; i < bestCombo.length; i++) {
            String guys = "";

            for (int j = 0; j < bestCombo[i].size(); j++) {
                Player player = (Player) bestCombo[i].get(j);
                guys += player.getName() + "(" + player.getElo() + ") ";
            }

            event.getChannel().sendMessage("> Team " + (i+1) + ":  " + guys).queue();
        }

        event.getChannel().sendMessage("> Team 1 Elo: " + teamOneClosestElo).queue();
        event.getChannel().sendMessage("> Team 2 Elo: " + teamTwoClosestElo).queue();

        return bestCombo;
    }

    public static List[] split(List<Player> list) {

        int size = list.size();

        List<Player> firstTeam = new ArrayList<>(list.subList(0, (size / 2)));
        List<Player> secondTeam = new ArrayList<>(list.subList((size / 2), size));

        return new List[]{firstTeam, secondTeam};
    }

    public int getTotalElo(List<Player> players) {
        int total = 0;
        for (Player player : players) {
            total += player.getElo();
        }

        return total;
    }

    public List<Player> addingPlayersToList(String[] playersFromChat) {
        List<Player> allPlayers = dao.getPlayers();
        List<Player> playerList = new ArrayList<>();

        //Adding all the players to list
        for (String player : playersFromChat) {
            boolean playerExists = false;
            for (Player newPlayer : allPlayers) {
                if (player.equalsIgnoreCase(newPlayer.getName())) {
                    playerList.add(newPlayer);
                    System.out.println("FOund: " + newPlayer.getName());
                    playerExists = true;
                }
            }
            if (!playerExists) {
//                dao.savePlayer(new Player(player, 100));
                playerList.add(new Player(player, 100));
            }
        }
        return playerList;
    }
}
