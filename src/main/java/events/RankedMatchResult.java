package events;

import elo.ELOMatch;
import elo.EntityDao;
import elo.entitities.Game;
import elo.entitities.Player;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.swing.text.html.parser.Entity;
import java.util.List;

public class RankedMatchResult extends ListenerAdapter {
    EntityDao dao = new EntityDao();

    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] messageSent = event.getMessage().getContentRaw().split(" ");

        if (messageSent[0].equalsIgnoreCase(".winner")) {


            Long gameId = Long.parseLong(messageSent[1]);
            Long winner = Long.parseLong(messageSent[2]);

            Game game = dao.getGameById(gameId);

            if (!game.isGameDone()) {
                if (winner == 1) {
                    List<Player> winners = game.getTeamOnePlayers();
                    List<Player> losers = game.getTeamTwoPlayers();
                    results(winners, losers, event, game);
                    game.setGameDone(true);
                    dao.updateGame(game);
                } else if (winner == 2) {
                    List<Player> winners = game.getTeamTwoPlayers();
                    List<Player> losers = game.getTeamOnePlayers();
                    results(winners, losers, event, game);
                    game.setGameDone(true);
                    dao.updateGame(game);
                } else {
                    event.getChannel().sendMessage("> Wrong team won").queue();
                }
            } else {
                event.getChannel().sendMessage("> Game is already scored").queue();
            }
        }
    }

    public void results(List<Player> winners, List<Player> losers, GuildMessageReceivedEvent event, Game game) {
        ELOMatch eloMatch = new ELOMatch();
        for (Player player : winners) {
            eloMatch.addPlayer(player.getName(), 1, player.getElo());
        }
        for (Player player : losers) {
            eloMatch.addPlayer(player.getName(), 2, player.getElo());
        }
        eloMatch.calculateELOs();

        String results = "";
        for (Player player : winners) {
            results += player.getName() + " " + eloMatch.getELO(player.getName()) + "(" + eloMatch.getELOChange(player.getName()) + ") ";
            player.setElo(eloMatch.getELO(player.getName()));
            player.setWin(player.getWin()+1);
        }
        for (Player player : losers) {
            results += player.getName() + " " + eloMatch.getELO(player.getName()) + "(" + eloMatch.getELOChange(player.getName()) + ") ";
            player.setElo(eloMatch.getELO(player.getName()));
            player.setLost(player.getLost()+1);
        }
        dao.saveGame(game);
        event.getChannel().sendMessage("> " + results).queue();
    }
}
