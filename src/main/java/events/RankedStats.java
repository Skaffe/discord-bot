package events;

import elo.EntityDao;
import elo.entitities.Game;
import elo.entitities.Player;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

public class RankedStats extends ListenerAdapter {
    EntityDao dao = new EntityDao();

    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] messageSent = event.getMessage().getContentRaw().split(" ");

        if (messageSent[0].equalsIgnoreCase(".stats")) {
            if (!event.getMember().getUser().isBot()) {
                Optional<Player> player;
                if(messageSent.length == 1){
                     player = getPlayer(event.getMember().getAsMention());
                }else{
                     player = getPlayer(messageSent[1]);
                }
                if(player.isPresent()){
                    event.getChannel().sendMessage("> " + player.get().getName() + ": " + player.get().getWin()+ "-" + player.get().getLost() + " (" + player.get().getElo() + ")").queue();
                }else{
                    event.getChannel().sendMessage("> No such player");
                }

            }
        }
    }

    public Optional<Player> getPlayer(String name){
        List<Player> players = dao.getPlayers();

        for (Player player : players) {
            if (name.equals(player.getName())) {
                return Optional.of(player);
            }
        }
        return null;
    }
}